<?php
// Before removing this file, please verify the PHP ini setting `auto_prepend_file` does not point to this.

// This file was the current value of auto_prepend_file during the Wordfence WAF installation (Sun, 26 Jun 2016 19:07:25 +0000)
if (file_exists('/usr/local/reserved/prepend.php')) {
	include_once '/usr/local/reserved/prepend.php';
}
if (file_exists('/sites/marceltenhave.nl/www/cms/wp-content/plugins/wordfence/waf/bootstrap.php')) {
	define("WFWAF_LOG_PATH", '/sites/marceltenhave.nl/www/cms/wp-content/wflogs/');
	include_once '/sites/marceltenhave.nl/www/cms/wp-content/plugins/wordfence/waf/bootstrap.php';
}
?>